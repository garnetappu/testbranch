package com.sample.test;

import org.openqa.selenium.WebDriver;

public class MainPage extends CommonTests{
	 public MainPage(WebDriver driver) {
	        super(driver);
	    }
	 
	    public void clickTab(String tab) {
	        click(tab);
	    }

}
