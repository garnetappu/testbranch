package com.sample.runtest;

import cucumber.api.CucumberOptions;
import cucumber.api.testng.AbstractTestNGCucumberTests;

@CucumberOptions(plugin = { "html:target/cucumber-html-report",
        "json:target/cucumber-dry.json", "pretty:target/cucumber-pretty-dry.txt",
        "usage:target/cucumber-usage-dry.json", "junit:target/cucumber-results-dry.xml" },
		features = "src/test/resources/", 
glue = "com.sample.test",
format = {"pretty",
		 "html:target/cucumber"
	//	   "html:target/site/cucumber-pretty",
		//   "rerun:target/rerun.txt",
	//	   "json:target/cucumber1.json"
		 })

public class RunTest extends AbstractTestNGCucumberTests {

}

